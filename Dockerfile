FROM node:lts

USER node
ENV NODE_ENV=production
WORKDIR /home/node/app/
COPY --chown=node:node app/ /home/node/app/
RUN npm clean-install
CMD ["node", "app.js"]
